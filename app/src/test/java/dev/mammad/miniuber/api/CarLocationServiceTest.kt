package dev.mammad.miniuber.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit

@RunWith(JUnit4::class)
class CarLocationServiceTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: CarLocationService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
            .build()
            .create(CarLocationService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun requestVehicles() {
        runBlocking {
            enqueueResponse("carLocations.json")
            val resultResponse = service.getCarData().body()

            val request = mockWebServer.takeRequest()
            assertNotNull(resultResponse)
            assertThat(request.path, `is`("/document.json"))
        }
    }

    @Test
    fun getVehicleListResponse() {
        runBlocking {
            enqueueResponse("carLocations.json")
            val resultResponse = service.getCarData().body()
            val vehicles = resultResponse!!.vehicles

            assertThat(vehicles.size, `is`(5))
        }
    }

    @Test
    fun getVehicleItem() {
        runBlocking {
            enqueueResponse("carLocations.json")
            val resultResponse = service.getCarData().body()
            val vehicles = resultResponse!!.vehicles

            val vehicle = vehicles[0]
            assertThat(vehicle.bearing, `is`(54))
            assertThat(vehicle.lat, `is`(35.7575154))
            assertThat(vehicle.lng, `is`(51.4104956))
            assertThat(vehicle.type, `is`("ECO"))
            assertThat(vehicle.imageUrl, `is`("https://snapp.ir/assets/test/snapp_map@2x.png"))
        }
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
            .getResourceAsStream("api-response/$fileName")
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(
            mockResponse.setBody(
                source.readString(Charsets.UTF_8)
            )
        )
    }
}
