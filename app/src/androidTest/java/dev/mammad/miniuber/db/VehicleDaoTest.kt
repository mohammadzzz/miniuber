package dev.mammad.miniuber.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import dev.mammad.miniuber.db.dao.VehicleDao
import dev.mammad.miniuber.db.util.getValue
import dev.mammad.miniuber.db.util.testVehicle1
import dev.mammad.miniuber.db.util.testVehicle2
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class VehicleDaoTest : DbTest() {
    private lateinit var vehicleDao: VehicleDao
    private val vehicle1 = testVehicle1.copy()
    private val vehicle2 = testVehicle2.copy()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        vehicleDao = db.vehicleDao()

        runBlocking {
            vehicleDao.insert(listOf(vehicle1, vehicle2))
        }
    }

    @Test
    fun testGetVehicles() {
        val list = getValue(vehicleDao.getVehicles())
        assertThat(list.size, equalTo(2))

        assertThat(list[0], equalTo(vehicle1))
        assertThat(list[1], equalTo(vehicle2))
    }

    @Test
    fun testGetVehicle() {
        assertThat(getValue(vehicleDao.getVehicles())[0], equalTo(vehicle1))
    }
}