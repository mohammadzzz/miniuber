package dev.mammad.miniuber.db.util

import dev.mammad.miniuber.ui.list.data.Vehicle

val testVehicle1 = Vehicle(100, "imageUrl", 35.7570448, 51.4094662, "ECO")

val testVehicle2 = Vehicle(200, "imageUrl", 35.7580966, 51.4091973, "PLUS")
