package dev.mammad.miniuber.api

import retrofit2.Response
import retrofit2.http.GET

interface CarLocationService {
    @GET("document.json")
    suspend fun getCarData(): Response<CarDataResponse>
}
