package dev.mammad.miniuber.api

import dev.mammad.miniuber.ui.list.data.Vehicle
import kotlinx.serialization.Serializable

@Serializable
data class CarDataResponse(
    val vehicles: List<Vehicle>
)