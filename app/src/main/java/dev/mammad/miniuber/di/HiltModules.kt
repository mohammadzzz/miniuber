package dev.mammad.miniuber.di

import android.app.Application
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dev.mammad.miniuber.BuildConfig
import dev.mammad.miniuber.api.CarLocationService
import dev.mammad.miniuber.db.AppDatabase
import dev.mammad.miniuber.db.dao.VehicleDao
import dev.mammad.miniuber.ui.list.data.VehicleDataSource
import dev.mammad.miniuber.ui.list.data.VehiclesRepository
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object HiltModules {
    @Provides
    @Singleton
    fun provideBaseUrl() = BuildConfig.BASE_URL

    @Provides
    @Singleton
    @UnstableDefault
    fun provideRetrofit(BASE_URL: String): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
            .retryOnConnectionFailure(true).addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideVehicleService(retrofit: Retrofit): CarLocationService =
        retrofit.create(CarLocationService::class.java)

    @Singleton
    @Provides
    fun provideVehicleDao(appDatabase: AppDatabase): VehicleDao =
        appDatabase.vehicleDao()

    @Singleton
    @Provides
    fun provideDb(app: Application) = AppDatabase.getInstance(app)

    @Singleton
    @Provides
    fun provideCarLocationDataSource(carLocationService: CarLocationService) =
        VehicleDataSource(carLocationService)

    @Singleton
    @Provides
    fun provideVehiclesRepository(
        vehicleDao: VehicleDao,
        vehicleDataSource: VehicleDataSource
    ) =
        VehiclesRepository(vehicleDao, vehicleDataSource)
}