package dev.mammad.miniuber.ui.list.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import dagger.hilt.android.AndroidEntryPoint
import dev.mammad.miniuber.R
import dev.mammad.miniuber.data.Result.Status.*
import dev.mammad.miniuber.databinding.FragmentVehiclesBinding
import dev.mammad.miniuber.ui.VerticalItemDecoration
import dev.mammad.miniuber.util.hide
import dev.mammad.miniuber.util.show

@AndroidEntryPoint
class ListFragment : Fragment() {

    companion object {
        fun newInstance() = ListFragment()
    }

    private val viewModel: ListViewModel by viewModels()
    lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentVehiclesBinding.inflate(inflater, container, false)
        context ?: return binding.root

        val adapter = VehicleListAdapter()
        binding.recyclerView.addItemDecoration(
            VerticalItemDecoration(resources.getDimension(R.dimen.margin_normal).toInt(), true)
        )
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.setEmptyView(binding.emptyView)
        binding.recyclerView.adapter = adapter

        subscribeUi(binding, adapter)

        setHasOptionsMenu(true)
        return binding.root
    }

    private fun subscribeUi(binding: FragmentVehiclesBinding, adapter: VehicleListAdapter) {
        viewModel.vehicles.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                SUCCESS -> {
                    binding.progressBar.hide()
                    it.data?.let { data ->
                        adapter.submitList(data)
                    }
                }
                LOADING -> binding.progressBar.show()
                ERROR -> {
                    binding.progressBar.hide()
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

    }
}