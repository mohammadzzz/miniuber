package dev.mammad.miniuber.ui.list.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.mammad.miniuber.binding.bindImageFromUrl
import dev.mammad.miniuber.databinding.ListItemBinding
import dev.mammad.miniuber.ui.list.data.Vehicle

class VehicleListAdapter : ListAdapter<Vehicle, VehicleListAdapter.ViewHolder>(DiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val vehicle = getItem(position)
        holder.apply {
            bind(vehicle)
            itemView.tag = vehicle
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    class ViewHolder(
        private val binding: ListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Vehicle) {
            binding.apply {
                vehicle = item
                bindImageFromUrl(image, item.imageUrl)
                executePendingBindings()
            }
        }
    }
}

private class DiffCallback : DiffUtil.ItemCallback<Vehicle>() {

    override fun areItemsTheSame(oldItem: Vehicle, newItem: Vehicle): Boolean {
        return oldItem.bearing == newItem.bearing
    }

    override fun areContentsTheSame(oldItem: Vehicle, newItem: Vehicle): Boolean {
        return oldItem == newItem
    }
}