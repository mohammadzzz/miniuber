package dev.mammad.miniuber.ui.list.data

import dev.mammad.miniuber.data.resultLiveData
import dev.mammad.miniuber.db.dao.VehicleDao
import javax.inject.Inject

class VehiclesRepository @Inject constructor(
    private val dao: VehicleDao,
    private val remoteSource: VehicleDataSource
) {

    val vehicles = resultLiveData(
        databaseQuery = { dao.getVehicles() },
        networkCall = { remoteSource.fetchData() },
        saveCallResult = { dao.insert(it.vehicles) })


}
