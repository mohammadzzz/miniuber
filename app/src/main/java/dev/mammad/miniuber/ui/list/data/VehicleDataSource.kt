package dev.mammad.miniuber.ui.list.data

import dev.mammad.miniuber.api.BaseDataSource
import dev.mammad.miniuber.api.CarLocationService
import javax.inject.Inject

class VehicleDataSource @Inject constructor(private val service: CarLocationService) :
    BaseDataSource() {
    suspend fun fetchData() = getResult { service.getCarData() }

}