package dev.mammad.miniuber.ui.map.ui

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import dev.mammad.miniuber.R
import dev.mammad.miniuber.data.Result
import dev.mammad.miniuber.ui.list.data.Vehicle
import dev.mammad.miniuber.util.toBitmap
import kotlinx.coroutines.*
import timber.log.Timber
import java.net.URL

@AndroidEntryPoint
class MapFragment : Fragment() {

    val viewModel: MapViewModel by viewModels()
    private val callback = OnMapReadyCallback { googleMap ->

        subscribeUi(googleMap)
    }

    private fun subscribeUi(googleMap: GoogleMap) {
        viewModel.vehicles.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.SUCCESS -> {
                    loadMarkers(it.data, googleMap)
                }
                Result.Status.LOADING -> Timber.d("Loading...")
                Result.Status.ERROR -> {
                    Timber.d(it.message)
                }
            }
        })
    }

    private fun loadMarkers(
        data: List<Vehicle>?,
        googleMap: GoogleMap
    ) {
        data?.map {
            val location = LatLng(it.lat, it.lng)
            val marker = MarkerOptions().position(location)
            addMarkerFromURL(it.imageUrl, googleMap, marker)
        }
    }

    fun addMarkerFromURL(url: String, googleMap: GoogleMap, marker: MarkerOptions) {
        val result: Deferred<Bitmap?> = GlobalScope.async {
            val imageUrl = URL(url)
            imageUrl.toBitmap()
        }

        GlobalScope.launch(Dispatchers.Main) {
            googleMap.addMarker(marker)
                .setIcon(BitmapDescriptorFactory.fromBitmap(result.await()))
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.position, 15f))

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }
}