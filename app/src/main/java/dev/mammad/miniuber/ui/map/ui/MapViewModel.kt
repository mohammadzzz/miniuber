package dev.mammad.miniuber.ui.map.ui

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import dev.mammad.miniuber.ui.list.data.VehiclesRepository

class MapViewModel @ViewModelInject constructor(
    repository: VehiclesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val vehicles = repository.vehicles

}