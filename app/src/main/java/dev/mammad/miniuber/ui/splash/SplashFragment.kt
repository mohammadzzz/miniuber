package dev.mammad.miniuber.ui.splash

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import dev.mammad.miniuber.R
import dev.mammad.miniuber.util.ConnectivityUtil
import kotlinx.android.synthetic.main.fragment_splash.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashFragment : Fragment() {

    private val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    @SuppressLint("LogNotTimber")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityScope.launch {
            val animation: Animation =
                AnimationUtils.loadAnimation(context, R.anim.anim_scale_up)
            splash_image.startAnimation(animation)
            splash_image.visibility = View.VISIBLE
            delay(3000)
            if (ConnectivityUtil.isConnected(requireContext())) {
                startMap()
            } else {
                startList()
            }
        }
    }

    private fun startList() {
        findNavController().navigate(R.id.action_SplashFragment_to_ListFragment)
    }

    private fun startMap() {
        findNavController().navigate(R.id.action_SplashFragment_to_MapFragment)
    }
}