package dev.mammad.miniuber.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import android.widget.ProgressBar
import java.io.IOException
import java.net.URL

fun ProgressBar.hide() {
    visibility = View.GONE
}

fun ProgressBar.show() {
    visibility = View.VISIBLE
}

fun URL.toBitmap(): Bitmap? {
    return try {
        BitmapFactory.decodeStream(openStream())
    } catch (e: IOException) {
        null
    }
}