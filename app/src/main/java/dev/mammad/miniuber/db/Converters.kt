package dev.mammad.miniuber.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dev.mammad.miniuber.ui.list.data.Vehicle
import java.lang.reflect.Type
import java.util.*


/**
 * Type converters to allow Room to reference complex data types.
 */
class Converters {
    @TypeConverter
    fun vehicleToJson(vehicles: List<Vehicle>): String = gson.toJson(vehicles).toString()

    @TypeConverter
    fun jsonToVehicleList(json: String?): List<Vehicle?>? {
        if (json == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<Vehicle?>?>() {}.type
        return gson.fromJson<List<Vehicle?>>(json, listType)
    }

    companion object {
        val gson = Gson()
    }
}