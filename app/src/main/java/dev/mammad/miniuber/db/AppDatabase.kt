package dev.mammad.miniuber.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import dagger.hilt.android.qualifiers.ApplicationContext
import dev.mammad.miniuber.db.dao.VehicleDao
import dev.mammad.miniuber.ui.list.data.Vehicle
import dev.mammad.miniuber.util.Consts.DATABASE_FILENAME

/**
 * The Room database for this app
 */
@Database(
    entities = [Vehicle::class],
    version = 2,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun vehicleDao(): VehicleDao

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: AppDatabase? = null
        fun getInstance(@ApplicationContext context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance
                    ?: buildDatabase(
                        context
                    )
                        .also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_FILENAME)
                .fallbackToDestructiveMigrationOnDowngrade()
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}
