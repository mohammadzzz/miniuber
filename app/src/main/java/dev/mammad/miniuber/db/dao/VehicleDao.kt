package dev.mammad.miniuber.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import dev.mammad.miniuber.ui.list.data.Vehicle

/**
 * The Data Access Object for the Vehicle class.
 */
@Dao
interface VehicleDao : BaseDao<Vehicle> {

    @Query("SELECT * FROM vehicle")
    fun getVehicles(): LiveData<List<Vehicle>>

}
